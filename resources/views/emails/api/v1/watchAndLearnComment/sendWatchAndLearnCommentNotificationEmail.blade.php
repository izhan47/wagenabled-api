<!DOCTYPE html>
<html lang="en">
    <head>
        <head>
            <title>Wag enabled watch and learn comment notification mail</title>
        </head>
    </head>
    <body>
        <p>Add new comment is made on the <a href="{{ $url }}" target="_blank">{{ $watchAndLearn->title }}</a> blog.</p>        
        <p>If the above link does not work please use this link:</p>
        <p>{{$url}}</p>

        <br/>
        <p>Warm Regards,</p>
        <p>The Wag Enabled Team</p>            
    </body>
</html>